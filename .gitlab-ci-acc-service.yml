stages:
  - test
  - build
  - build docker
  - deploy

variables:
  APP_NAME: accounts-service
  CLUSTER_NAME: cluster-casino
  SERVICE_NAME: service-accounts
  TASK_NAME: task-accounts
  ARN_TASK_ROLE: arn:aws:iam::259618562134:role/ecsTaskExecutionRole
  STG_TASK_DEFINITION_MEMORY: 2048
  STG_TASK_DEFINITION_CPU: 1024
  PRD_TASK_DEFINITION_MEMORY: 4096
  PRD_TASK_DEFINITION_CPU: 2048
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
  GIT_DEPTH: "0"

sonarcloud-check:
  stage: test
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  allow_failure: true
  script:
    - sonar-scanner
  only:
    - development
    - stage
    - master

build nestjs:
  stage: build
  image: node:18
  allow_failure: false
  only:
    - stage
    - master
  before_script:
    - npm config set -- '//${CI_SERVER_HOST}/api/v4/packages/npm/:_authToken' "${CI_JOB_TOKEN}"
    - npm i
  script:
    - npm run build
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - dist/

build docker image:
  stage: build docker
  image: docker:20.10.22
  services:
    - docker:dind
  allow_failure: false
  only:
    - stage
    - master
  before_script:
    - apk add --no-cache aws-cli
  script:
    # Build docker image
    - DOCKER_REGISTRY_URL=$(echo "$DOCKER_REGISTRY/$APP_NAME:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA")
    - echo $DOCKER_REGISTRY_URL
    - docker build -t $DOCKER_REGISTRY_URL .
    - aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $DOCKER_REGISTRY
    - docker push $DOCKER_REGISTRY_URL
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - dist/

.deploy-ecs:
  stage: deploy
  image: docker:20.10.22
  services:
    - docker:dind
  allow_failure: false
  only:
    - stage
  before_script:
    - apk add --no-cache aws-cli
    - apk add --no-cache jq
  script:
    # Update environment variables
    - chmod +x scripts/${CI_ENVIRONMENT_NAME}-create-envs.sh
    - scripts/${CI_ENVIRONMENT_NAME}-create-envs.sh
    - echo Upload env file to s3...
    - aws s3 cp .env s3://$CI_ENVIRONMENT_NAME-casino-envs/services/$APP_NAME/
    # Update task definition
    - echo Update task definition...
    - DOCKER_REGISTRY_URL=$(echo "$DOCKER_REGISTRY/$APP_NAME:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA")
    - TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition "$CI_ENVIRONMENT_NAME-$TASK_NAME" --region "${AWS_DEFAULT_REGION}")
    - NEW_CONTAINER_DEFINTION=$(echo $TASK_DEFINITION | jq --arg IMAGE "$DOCKER_REGISTRY_URL" '.taskDefinition.containerDefinitions[0].image = $IMAGE | .taskDefinition.containerDefinitions[0]')
    - echo "Registering new container definition..."
    - aws ecs register-task-definition --region $AWS_DEFAULT_REGION --family "$CI_ENVIRONMENT_NAME-${TASK_NAME}" --container-definitions "${NEW_CONTAINER_DEFINTION}" --memory $TASK_DEFINITION_MEMORY --cpu $TASK_DEFINITION_CPU --task-role-arn $ARN_TASK_ROLE --execution-role-arn $ARN_TASK_ROLE --requires-compatibilities "FARGATE" --network-mode "awsvpc"
    # Update service
    - echo "Updating the service..."
    - aws ecs update-service --region $AWS_DEFAULT_REGION --cluster "$CI_ENVIRONMENT_NAME-${CLUSTER_NAME}" --service "$CI_ENVIRONMENT_NAME-${SERVICE_NAME}" --task-definition "$CI_ENVIRONMENT_NAME-${TASK_NAME}"
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
      - dist/

deploy stg:
  extends: .deploy-ecs
  only:
    - stage
  environment:
    name: stg
    url: https://api-stg.lasvegasenvivo.com/accounts
  variables:
    TASK_DEFINITION_MEMORY: $STG_TASK_DEFINITION_MEMORY
    TASK_DEFINITION_CPU: $STG_TASK_DEFINITION_CPU

deploy prd:
  extends: .deploy-ecs
  only:
    - master
  environment:
    name: prd
    url: https://api.lasvegasenvivo.com/accounts
  variables:
    TASK_DEFINITION_MEMORY: $PRD_TASK_DEFINITION_MEMORY
    TASK_DEFINITION_CPU: $PRD_TASK_DEFINITION_CPU
